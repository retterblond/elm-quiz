module Main exposing (main)

import Browser exposing (UrlRequest)
import Browser.Navigation as Nav exposing (Key)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as D exposing (Decoder)
import Json.Decode.Pipeline as DP
import Process
import RemoteData exposing (RemoteData(..), WebData, fromResult)
import Task
import Time exposing (Posix, millisToPosix)
import Url
import Url.Parser exposing ((<?>))
import Url.Parser.Query


type Msg
    = SetQuery String
    | URLRequest UrlRequest
    | UrlChange Url.Url
    | GetHeroes
    | GotHeroes (WebData Response)
    | Debounce Posix


type alias Flags =
    {}


main : Program Flags Model Msg
main =
    Browser.application
        { view = view
        , init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        , onUrlRequest = URLRequest
        , onUrlChange = UrlChange
        }



-- HELPERS


parseQuery : Url.Url -> Maybe String
parseQuery url =
    url
        |> Debug.log "url"
        |> Url.Parser.parse (Url.Parser.top <?> Url.Parser.Query.string "q")
        |> Maybe.andThen identity


startDebounce : Cmd Msg
startDebounce =
    Task.perform Debounce Time.now


extractHeroes : Response -> String -> WebData Heroes
extractHeroes response query =
    if response.query == query then
        Success response.results
    else
        Failure (Http.BadBody "mismatch in query response")



-- CONSTANTS


debounceDelay : Posix
debounceDelay =
    millisToPosix 100



-- MODEL


type alias Heroes =
    List Hero


type alias Hero =
    { id : Int
    , name : String
    , username : String
    }


type alias Model =
    { query : String
    , heroes : WebData Heroes
    , key : Key
    , requestTime : Posix
    }


type alias Response =
    { count : Int
    , query : String
    , results : Heroes
    }


init : Flags -> Url.Url -> Key -> ( Model, Cmd Msg )
init flags location key =
    let
        query =
            Maybe.withDefault "" (parseQuery location)
    in
    ( { query = query
      , heroes = Loading
      , key = key
      , requestTime = Time.millisToPosix 0
      }
    , getHeroes query
    )



-- UPDATE


nextUrl : String -> String
nextUrl query =
    if String.length query == 0 then
        "/"

    else
        "/?q=" ++ Url.percentEncode query


update : Msg -> Model -> ( Model, Cmd Msg )
update msg_ model_ =
    { model = model_, msg = msg_ }
        |> Debug.log "model, msg: "
        |> (\{ model, msg } ->
                case msg of
                    SetQuery query ->
                        ( model
                        , Nav.pushUrl model.key (nextUrl query)
                        )

                    URLRequest urlRequest ->
                        ( model, Cmd.none )

                    UrlChange url ->
                        ( { model
                            | query = Maybe.withDefault "" (parseQuery url)
                            , heroes = Loading
                          }
                        , startDebounce
                        )

                    GetHeroes ->
                        ( model, getHeroes model.query )

                    GotHeroes data ->
                        ( { model | heroes = RemoteData.andThen (\a -> extractHeroes a model.query) data}
                        , Cmd.none
                        )

                    Debounce now ->
                        if Time.posixToMillis now - Time.posixToMillis model.requestTime > Time.posixToMillis debounceDelay then
                            ( { model | requestTime = now }
                            , Process.sleep (toFloat <| Time.posixToMillis debounceDelay) |> Task.perform (always GetHeroes)
                            )

                        else
                            ( model, Cmd.none )
           )



-- VIEW


view : Model -> Browser.Document Msg
view { query, heroes } =
    { title = ""
    , body =
        [ div [ class "app" ]
            [ viewSearchInput query
            , viewheroesWebData heroes
            ]
        ]
    }


viewSearchInput : String -> Html Msg
viewSearchInput query =
    input
        [ type_ "search"
        , placeholder "Search heroes..."
        , class "search-input"
        , value query
        , onInput SetQuery
        ]
        [ text query ]


viewheroesWebData : WebData Heroes -> Html Msg
viewheroesWebData heroesWebData =
    case heroesWebData of
        NotAsked ->
            Html.p [] [ text "Not Asked." ]

        Loading ->
            Html.p [] [ text "Loading." ]

        Failure error ->
            Html.p [] [ text ("Error: " ++ Debug.toString error) ]

        Success heroes ->
            Html.ul [] (List.map (\hero -> Html.li [] [ text (hero.name ++ ", " ++ hero.username) ]) heroes)



-- HTTP


heroesUrl : String -> String
heroesUrl query =
    "http://localhost:8000/api/heroes?q=" ++ query


getHeroes : String -> Cmd Msg
getHeroes query =
    Http.get
        { url = heroesUrl query
        , expect =
            Http.expectJson (fromResult >> GotHeroes)                                 
                (D.map3 Response
                    (D.field "count" D.int)
                    (D.field "query" D.string)
                    (D.field "results"
                        (D.list
                            (D.map3 Hero
                                (D.field "id" D.int)
                                (D.field "name" D.string)
                                (D.field "username" D.string)
                            )
                        )
                    )
                )
        }
